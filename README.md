Malayalam Corpus by Swathanthra Malayalam Computing
===================================================

This is a collection of Malayalam content collected from various sources and then curated and processed for general purpose usage.

License
-------

Creative Commons Attribution-ShareAlike https://creativecommons.org/licenses/by-sa/3.0/